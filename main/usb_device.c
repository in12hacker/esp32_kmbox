#include "ch374_usb.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"
#include "hid.h"
#include "driver/gpio.h"
#include <string.h>
#include "hid_report_descr_parser.h"

#define DEVICE_SPI_CS          16
#define DEVICE_SPI_CLK         17
#define DEVICE_SPI_MOSI        5
#define DEVICE_SPI_MISO        18
#define DEVICE_SPI_INT         4

#define CH374_READ_CMD     0xC0
#define CH374_WRITE_CMD    0x80

#if 0
// 设备描述符
const	uint8_t	MyDevDescr[] = { 0x12, 0x01, 0x10, 0x01,
								0xFF, 0x80, 0x37, 0x08,
								0x86, 0x1A, 0x37, 0x55,  // 厂商ID和产品ID
								0x00, 0x01, 0x01, 0x02,
								0x00, 0x01 };
// 配置描述符
const	uint8_t	MyCfgDescr[] = { 0x09, 0x02, 0x27, 0x00, 0x01, 0x01, 0x00, 0x80, 0x32,
								 0x09, 0x04, 0x00, 0x00, 0x03, 0xFF, 0x80, 0x37, 0x00,
								 0x07, 0x05, 0x82, 0x02, 0x40, 0x00, 0x00,
								 0x07, 0x05, 0x02, 0x02, 0x40, 0x00, 0x00,
								 0x07, 0x05, 0x81, 0x03, 0x08, 0x00, 0x00 };
#endif



// 设备描述符
const	uint8_t	MyDevDescr[] = { 0x12, 0x01, 0x10, 0x01,
								0x00, 0x00, 0x00, 0x08,
								0x88, 0x88, 0x01, 0x00,  // 厂商ID和产品ID
								0x00, 0x01, 0x01, 0x02,
								0x00, 0x01 };
#if 0
#if 0  //x,y轴范围127
// 鼠标配置描述符        如果有配置描述符长度改变，记得修改第2,3字节！！！！  
const	uint8_t	MyCfgDescr[9 + 9 + 9 +7] = { 0x09, 0x02, 0x22, 0x00, 0x01, 0x01, 0x00, 0x80, 0x32,  /* 配置描述符 */
                                             0x09, 0x04, 0x00, 0x00, 0x01, 0x03, 0x01, 0x02, 0x00,  /* 接口描述符 */
                                             0x09, 0x21, 0x10, 0x01, 0x21, 0x01, 0x22, 0x34, 0x00,  /* HID描述符  */
                                             0x07, 0x05, 0x81, 0x03, 0x10, 0x00, 0x0a};             /* 输入端点描述符 */
//鼠标报告描述符    如果报告描述符长度改变，记得修改HID描述符里的下级描述符长度字段！！！！
uint8_t ReportDescr[] =    {                
  //通用桌面设备
    0x05, 0x01, // USAGE_PAGE (Generic Desktop)
    //鼠标
    0x09, 0x02, // USAGE (Mouse)
    //集合
    0xa1, 0x01, // COLLECTION (Application)
    //指针设备
    0x09, 0x01, // USAGE (Pointer)
    //集合
    0xa1, 0x00, // COLLECTION (Physical)
    //按键
    0x05, 0x09, // USAGE_PAGE (Button)
    //使用最小值1
    0x19, 0x01, // USAGE_MINIMUM (Button 1)
    //使用最大值3。1表示左键，2表示右键，3表示中键
    0x29, 0x03, // USAGE_MAXIMUM (Button 3)
    //逻辑最小值0
    0x15, 0x00, // LOGICAL_MINIMUM (0)
    //逻辑最大值1
    0x25, 0x01, // LOGICAL_MAXIMUM (1)
    //数量为3
    0x95, 0x03, // REPORT_COUNT (3)
    //大小为1bit
    0x75, 0x01, // REPORT_SIZE (1)
    //输入，变量，数值，绝对值
    //以上3个bit分别表示鼠标的三个按键情况，最低位（bit-0）为左键
    //bit-1为右键，bit-2为中键，按下时对应的位值为1，释放时对应的值为0
    0x81, 0x02, // INPUT (Data,Var,Abs)
    //填充5个bit，补足一个字节
    0x95, 0x01, // REPORT_COUNT (1)
    0x75, 0x05, // REPORT_SIZE (5)
    0x81, 0x03, // INPUT (Cnst,Var,Abs)
    //用途页为通用桌面
    0x05, 0x01, // USAGE_PAGE (Generic Desktop)
    //用途为X
    0x09, 0x30, // USAGE (X)
    //用途为Y
    0x09, 0x31, // USAGE (Y)
    //用途为滚轮
    0x09, 0x38, // USAGE (Wheel)
    //逻辑最小值为-127
    0x15, 0x81, // LOGICAL_MINIMUM (-127)
    //逻辑最大值为+127
    0x25, 0x7f, // LOGICAL_MAXIMUM (127)
    //大小为8个bits
    0x75, 0x08, // REPORT_SIZE (8)
    //数量为3个，即分别代表x,y,滚轮
    0x95, 0x03, // REPORT_COUNT (3)
    //输入，变量，值，相对值
    0x81, 0x06, // INPUT (Data,Var,Rel)
    //关集合
    0xc0, // END_COLLECTION
    0xc0 // END_COLLECTION
    };
#else  //x,y轴范围32767
// 鼠标配置描述符        如果有配置描述符长度改变，记得修改第2,3字节！！！！  
const	uint8_t	MyCfgDescr[9 + 9 + 9 +7] = { 0x09, 0x02, 0x22, 0x00, 0x01, 0x01, 0x00, 0x80, 0x32,  /* 配置描述符 */
                                             0x09, 0x04, 0x00, 0x00, 0x01, 0x03, 0x01, 0x02, 0x00,  /* 接口描述符 */
                                             0x09, 0x21, 0x10, 0x01, 0x21, 0x01, 0x22, 0x30, 0x00,  /* HID描述符  */
                                             0x07, 0x05, 0x81, 0x03, 0x10, 0x00, 0x0a};             /* 输入端点描述符 */
//鼠标报告描述符    如果报告描述符长度改变，记得修改HID描述符里的下级描述符长度字段！！！！
uint8_t ReportDescr[] =    {                
0x05, 0x01,        // Usage Page (Generic Desktop Ctrls)
0x09, 0x02,        // Usage (Mouse)
0xA1, 0x01,        // Collection (Application)
#if 0
0x85, 0x01,        //   Report ID (66)
#endif
0x09, 0x01,        //   Usage (Pointer)
0xA1, 0x00,        //   Collection (Physical)
0x05, 0x09,        //     Usage Page (Button)
0x19, 0x01,        //     Usage Minimum (0x01)
0x29, 0x08,        //     Usage Maximum (0x08)
0x15, 0x00,        //     Logical Minimum (0)
0x25, 0x01,        //     Logical Maximum (1)
0x95, 0x08,        //     Report Count (8)
0x75, 0x01,        //     Report Size (1)
0x81, 0x02,        //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
0x05, 0x01,        //     Usage Page (Generic Desktop Ctrls)
0x16, 0x01, 0x80,  //     Logical Minimum (-32767)
0x26, 0xFF, 0x7F,  //     Logical Maximum (32767)
0x09, 0x30,        //     Usage (X)
0x09, 0x31,        //     Usage (Y)
0x09, 0x38,        //     Usage (Wheel)
0x75, 0x10,        //     Report Size (16)
0x95, 0x03,        //     Report Count (3)
0x81, 0x06,        //     Input (Data,Var,Rel,No Wrap,Linear,Preferred State,No Null Position)
0xC0,              //   End Collection
0xC0,              // End Collection
};
#endif
#endif

#if 0
//键盘配置描述符    如果有配置描述符长度改变，记得修改第2,3字节！！！！                                          
const	uint8_t	MyCfgDescr[9 + 9 + 9 + 7 + 7] = { 0x09, 0x02, 0x29, 0x00, 0x01, 0x01, 0x00, 0x80, 0x32,  /* 配置描述符 */
                                             0x09, 0x04, 0x00, 0x00, 0x02, 0x03, 0x01, 0x01, 0x00,  /* 接口描述符 */
                                             0x09, 0x21, 0x10, 0x01, 0x21, 0x01, 0x22, 0x41, 0x00,  /* HID描述符  */
                                             0x07, 0x05, 0x81, 0x03, 0x10, 0x00, 0x01,              /* 输入端点描述符 */
                                             0x07, 0x05, 0x01, 0x03, 0x10, 0x00, 0x01};             /* 输出端点描述符 */
//键盘报告描述符    如果报告描述符长度改变，记得修改HID描述符里的下级描述符长度字段！！！！
uint8_t ReportDescr[] =    {
    0x05, 0x01,
    0x09, 0x06,
    0xa1, 0x01,
    0x05, 0x07,
    0x19, 0xe0,
    0x29, 0xe7,
    0x15, 0x00,
    0x25, 0x01,
    0x95, 0x08,
    0x75, 0x01,
    0x81, 0x02,
    0x95, 0x01,
    0x75, 0x08,
    0x81, 0x03,
    0x95, 0x06,
    0x75, 0x08,
    0x15, 0x00,
    0x25, 0xff,
    0x05, 0x07,
    0x19, 0x00,
    0x29, 0x65,
    0x81, 0x00,
    0x25, 0x01,
    0x95, 0x05,
    0x75, 0x01,
    0x05, 0x08,
    0x19, 0x01,
    0x29, 0x05,
    0x91, 0x02,
    0x95, 0x01,
    0x75, 0x03,
    0x91, 0x03,
    0xc0,
};
#endif

#if 1  //带鼠标功能的USB键盘
//配置描述符    如果有配置描述符长度改变，记得修改第2,3字节！！！！                                          
const	uint8_t	MyCfgDescr[9 + 9 + 9 + 7 + 7] = { 0x09, 0x02, 0x29, 0x00, 0x01, 0x01, 0x00, 0x80, 0x32,  /* 配置描述符 */
                                             0x09, 0x04, 0x00, 0x00, 0x02, 0x03, 0x01, 0x01, 0x00,  /* 接口描述符 */
                                             0x09, 0x21, 0x10, 0x01, 0x21, 0x01, 0x22, 0x75, 0x00,  /* HID描述符  */
                                             0x07, 0x05, 0x82, 0x03, 0x10, 0x00, 0x0a,              /* 输入端点描述符 使用ep2的in端点，因为ep1的in端点大小只有8字节 */
                                             0x07, 0x05, 0x01, 0x03, 0x10, 0x00, 0x0a};             /* 输出端点描述符 使用ep1的out端点*/
//键盘报告描述符    如果报告描述符长度改变，记得修改HID描述符里的下级描述符长度字段！！！！
uint8_t ReportDescr[] =    {
    0x05, 0x01,
    0x09, 0x06,
    0xa1, 0x01,
    0x85, 0x01,
    0x05, 0x07,
    0x19, 0xe0,
    0x29, 0xe7,
    0x15, 0x00,
    0x25, 0x01,
    0x95, 0x08,
    0x75, 0x01,
    0x81, 0x02,
    0x95, 0x01,
    0x75, 0x08,
    0x81, 0x03,
    0x95, 0x06,
    0x75, 0x08,
    0x15, 0x00,
    0x25, 0xff,
    0x05, 0x07,
    0x19, 0x00,
    0x29, 0x65,
    0x81, 0x00,
    0x25, 0x01,
    0x95, 0x05,
    0x75, 0x01,
    0x05, 0x08,
    0x19, 0x01,
    0x29, 0x05,
    0x91, 0x02,
    0x95, 0x01,
    0x75, 0x03,
    0x91, 0x03,
    0xc0,
 
0x05, 0x01,        // Usage Page (Generic Desktop Ctrls)
0x09, 0x02,        // Usage (Mouse)
0xA1, 0x01,        // Collection (Application)
    0x85, 0x02,        //   Report ID (66)
    0x09, 0x01,        //   Usage (Pointer)
    0xA1, 0x00,        //   Collection (Physical)
        0x05, 0x09,        //     Usage Page (Button)
            0x19, 0x01,        //     Usage Minimum (0x01)
            0x29, 0x08,        //     Usage Maximum (0x08)
            0x15, 0x00,        //     Logical Minimum (0)
            0x25, 0x01,        //     Logical Maximum (1)
            0x95, 0x08,        //     Report Count (8)
            0x75, 0x01,        //     Report Size (1)
            0x81, 0x02,        //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
        0x05, 0x01,        //     Usage Page (Generic Desktop Ctrls)
            0x16, 0x01, 0x80,  //     Logical Minimum (-32767)
            0x26, 0xFF, 0x7F,  //     Logical Maximum (32767)
            0x09, 0x30,        //     Usage (X)
            0x09, 0x31,        //     Usage (Y)
            0x09, 0x38,        //     Usage (Wheel)
            0x75, 0x10,        //     Report Size (16)
            0x95, 0x03,        //     Report Count (3)
            0x81, 0x06,        //     Input (Data,Var,Rel,No Wrap,Linear,Preferred State,No Null Position)
    0xC0,              //   End Collection
0xC0,              // End Collection
};

#endif


// 语言描述符
const	uint8_t	MyLangDescr[] = { 0x04, 0x03, 0x09, 0x04 };
// 厂家信息
const	uint8_t	MyManuInfo[] = { 0x0E, 0x03, 'w', 0, 'c', 0, 'h', 0, '.', 0, 'c', 0, 'n', 0 };
// 产品信息
const	uint8_t	MyProdInfo[] = { 0x0C, 0x03, 'C', 0, 'H', 0, '3', 0, '7', 0, '4', 0 };

uint8_t	UsbConfig = 0;	// USB配置标志

#define EP2_IN_EVENT (0x01 << 0)

QueueHandle_t xQueue;
EventGroupHandle_t Event_Handle = NULL;
bool is_fake_km_ready = false;

void IRAM_ATTR usb_device_task(void *parm);
void ch374_device_port_init(void)
{
    gpio_config_t io_conf = {};

    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ( (1 << DEVICE_SPI_CS) | (1 << DEVICE_SPI_CLK) |
                            (1 << DEVICE_SPI_MOSI) );
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);  

    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = (1 << DEVICE_SPI_MISO) | (1 << DEVICE_SPI_INT);
    gpio_config(&io_conf); 

    io_conf.intr_type = GPIO_PIN_INTR_LOLEVEL;
    io_conf.pin_bit_mask = (1 << DEVICE_SPI_INT);
    io_conf.mode = GPIO_MODE_INPUT;
    gpio_config(&io_conf);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(DEVICE_SPI_INT, usb_device_task, (void*) DEVICE_SPI_INT);

    gpio_set_level(DEVICE_SPI_CS, 1);
    gpio_set_level(DEVICE_SPI_CLK, 0);
}

static void ch374_spi_cs(uint8_t level)
{
    gpio_set_level(DEVICE_SPI_CS, level);
}


static void ch374_write_data(uint8_t data)
{
    int8_t i;
    for(i = 7;i >= 0;i--){
        gpio_set_level(DEVICE_SPI_CLK, 0);
        gpio_set_level(DEVICE_SPI_MOSI, (data >> i) & 0x01);
        gpio_set_level(DEVICE_SPI_CLK, 1);
    }
}

static uint8_t ch374_read_data(void)
{
    int8_t i, data = 0;
    for(i = 7;i >= 0;i--){
        gpio_set_level(DEVICE_SPI_CLK, 0);
        data |= ( gpio_get_level(DEVICE_SPI_MISO) << i );
        gpio_set_level(DEVICE_SPI_CLK, 1);
    }
    return data;
}

static uint8_t ch374_query_interrupt(void)
{
    return !gpio_get_level(DEVICE_SPI_INT);
}

static void ch374_write_byte(uint8_t addr, uint8_t data)
{
    vTaskSuspendAll();
    ch374_spi_cs(0);

    /* 1. addr */
    ch374_write_data(addr);
    /* 2. write cmd */
    ch374_write_data(CH374_WRITE_CMD);
    /* 3. data */
    ch374_write_data(data);
    
    ch374_spi_cs(1);
    if (xTaskResumeAll() == pdTRUE)
    {
        taskYIELD();
    }
}

static uint8_t ch374_read_byte(uint8_t addr)
{
    uint8_t data;

    vTaskSuspendAll();

    ch374_spi_cs(0);

    /* 1. addr */
    ch374_write_data(addr);
    /* 2. read cmd */
    ch374_write_data(CH374_READ_CMD);
    /* 3. data */
    data = ch374_read_data();
    
    ch374_spi_cs(1);
    if (xTaskResumeAll() == pdTRUE)
    {
        taskYIELD();
    }

    return data;   
}

static void ch374_write_bytes(uint8_t addr, uint8_t *data, uint8_t len)
{
    uint8_t i;

    vTaskSuspendAll();

    ch374_spi_cs(0);

    /* 1. addr */
    ch374_write_data(addr);
    /* 2. write cmd */
    ch374_write_data(CH374_WRITE_CMD);
    /* 3. data */
    for(i = 0;i < len;i++){
                ch374_write_data(*(data + i));
    }
        
    ch374_spi_cs(1);
    if (xTaskResumeAll() == pdTRUE)
    {
        taskYIELD();
    }
}

static void ch374_read_bytes(uint8_t addr, uint8_t *data, uint8_t len)
{
    uint8_t i;

    vTaskSuspendAll();

    ch374_spi_cs(0);

    /* 1. addr */
    ch374_write_data(addr);
    /* 2. read cmd */
    ch374_write_data(CH374_READ_CMD);
    /* 3. data */
    for(i = 0;i < len;i++){
        data[i] = ch374_read_data();
    }
    
    ch374_spi_cs(1);
    if (xTaskResumeAll() == pdTRUE)
    {
        taskYIELD();
    }
}

void mouse_task(void *parm)
{
    //printf("mouse test!\n");

    vTaskDelay(3000 / portTICK_PERIOD_MS);
    ch374_write_bytes( RAM_ENDP1_TRAN, (uint8_t *)"\x00\x50\x00\x50\x00\x00\x00", 7); //x+50,y+50
    ch374_write_byte( REG_USB_LENGTH, 7 );
    ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 7 ) ^ BIT_EP1_RECV_TOG );

    // vTaskDelay(3000 / portTICK_PERIOD_MS);
    // ch374_write_bytes( RAM_ENDP1_TRAN, (uint8_t *)"\x02\x00\x00\x00", 4);  //右键down
    // ch374_write_byte( REG_USB_LENGTH, 4 );
    // ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 4 ) ^ BIT_EP1_RECV_TOG );

    // vTaskDelay(3000 / portTICK_PERIOD_MS);
    // ch374_write_bytes( RAM_ENDP1_TRAN, (uint8_t *)"\x00\x00\x00\x00", 4);  //右键up
    // ch374_write_byte( REG_USB_LENGTH, 4 );
    // ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 4 ) ^ BIT_EP1_RECV_TOG );

    // vTaskDelay(3000 / portTICK_PERIOD_MS);
    // ch374_write_bytes( RAM_ENDP1_TRAN, (uint8_t *)"\x01\x00\x00\x00", 4); //左键down
    // ch374_write_byte( REG_USB_LENGTH, 4 );
    // ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 4 ) ^ BIT_EP1_RECV_TOG );

    // vTaskDelay(3000 / portTICK_PERIOD_MS);
    // ch374_write_bytes( RAM_ENDP1_TRAN, (uint8_t *)"\x00\x00\x00\x00", 4); //左键up
    // ch374_write_byte( REG_USB_LENGTH, 4 );
    // ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 4 ) ^ BIT_EP1_RECV_TOG );

    vTaskDelete(NULL);
}

int g_flag = 0;
void keyboard_task(void *parm)
{
    uint8_t data[8] = {0};

    g_flag++;

    if(g_flag != 1)
        vTaskDelete(NULL);

    //printf("keyboard test!\n");

    vTaskDelay(3000 / portTICK_PERIOD_MS);
    data[2] = KB_H;   //h down
    ch374_write_bytes( RAM_ENDP1_TRAN, data, 8);
    // ch374_write_byte( REG_USB_LENGTH, 8 );
    ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 8 ) ^ BIT_EP1_RECV_TOG );

    vTaskDelay(10 / portTICK_PERIOD_MS);  //时间长了不释放，会按出多个该按键
    data[2] = 0x00;   //h up
    ch374_write_bytes( RAM_ENDP1_TRAN, data, 8);
    // ch374_write_byte( REG_USB_LENGTH, 8 );
    ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_ACK( ch374_read_byte( REG_USB_ENDP1 ), 8 ) ^ BIT_EP1_RECV_TOG );

    vTaskDelete(NULL);
}

uint8_t keyboard_mouse_data[9] = {0};


void km_report(struct report_msg msg);
void keyboard_with_mouse_task(void *parm)
{
    struct report_msg msg;
    g_flag++;

    if(g_flag != 1)
        vTaskDelete(NULL);

    memset(&msg, 0x0, sizeof(struct report_msg));
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    //printf("----------------------keyboard_with_mouse test!\n");

    int i;
    for(i = 0;i < 26;i++){
    keyboard_mouse_data[0] = 0x01;  //键盘
    keyboard_mouse_data[3] = KB_A;
    // ch374_write_bytes( RAM_ENDP2_TRAN, keyboard_mouse_data, 9);
    // ch374_write_byte( REG_USB_LENGTH, 9 );
    // ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_RECV_TOG );
    msg.type = keyboard_mouse_data[0];
    msg.data[2] = KB_A + i;
    km_report(msg);

    //vTaskDelay(100 / portTICK_PERIOD_MS);  //时间长了不释放，会按出多个该按键,时间短了会
    keyboard_mouse_data[3] = 0;
    // ch374_write_bytes( RAM_ENDP2_TRAN, keyboard_mouse_data, 9);
    // ch374_write_byte( REG_USB_LENGTH, 9 );
    // ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_RECV_TOG );
    msg.type = keyboard_mouse_data[0];
    msg.data[2] = 0;
    km_report(msg);
    }

    vTaskDelay(2000 / portTICK_PERIOD_MS);
    memset(keyboard_mouse_data, 0x0, 9);
    keyboard_mouse_data[0] = 0x02;   //鼠标   
    keyboard_mouse_data[2] = 50;   //x+50
    keyboard_mouse_data[4] = 50;   //x+50
    ch374_write_bytes( RAM_ENDP2_TRAN, keyboard_mouse_data, 9);
    ch374_write_byte( REG_USB_LENGTH, 9 );
    ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_RECV_TOG );

    vTaskDelete(NULL);
}

void IRAM_ATTR usb_device_task(void *parm)
{
    uint8_t i;
    uint8_t int_status, usb_status, usb_len;
    static	uint8_t	SetupReq, SetupLen, device_addr;
	static	uint8_t*	pDescr;
    bool ep2_in_intr_flag = false;
    BaseType_t xHigherPriorityTaskWoken, xResult;


    //while(1)
    {
        if(ch374_query_interrupt()){

            int_status = ch374_read_byte(REG_INTER_FLAG); // 获取中断状态
            usb_status = ch374_read_byte( REG_USB_STATUS );
            usb_len = ch374_read_byte( REG_USB_LENGTH );

            //printf("\n");

            if ( int_status & BIT_IF_BUS_RESET ) {  // USB总线复位
                //printf("usb bus reset\n");
                ch374_write_byte( REG_USB_ADDR, 0x00 );  // 清USB设备地址
                ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_NAK( 0 ) );
                ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_NAK( 0 ) );
                ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_NAK( 0 ) );
                ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_BUS_RESET );  // 清中断标志
            }else if ( int_status & BIT_IF_TRANSFER ) {  // USB传输完成
                //printf("usb translate complete\n");
                usb_status = ch374_read_byte( REG_USB_STATUS );
                switch( usb_status & BIT_STAT_PID_ENDP ) {  // USB设备中断状态
                    case USB_INT_EP2_OUT: {  // 批量端点下传成功 
                        //printf("bulk ep download ok \n");
                        uint8_t	buf[64];
                        if ( usb_status & BIT_STAT_TOG_MATCH ) {  // 仅同步包
                            usb_len = ch374_read_byte( REG_USB_LENGTH );
                            ch374_read_bytes( RAM_ENDP2_RECV, buf, usb_len);
                            for ( i = 0; i < usb_len; i ++ ){ 
                                buf[i] ^= 0xFF;  // 数据取反由计算机验证
                            }
                            ch374_write_bytes( RAM_ENDP2_TRAN, buf, usb_len);  // 演示回传
                            ch374_write_byte( REG_USB_LENGTH, usb_len );
                            ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_RECV_TOG );
                        }
                        break;
                    }

                    case USB_INT_EP2_IN: {  // 批量端点上传成功,未处理
                        ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_NAK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_TRAN_TOG );
                        ep2_in_intr_flag = true;
                        break;
                    }
                    case USB_INT_EP1_IN: {  // 中断端点上传成功,未处理
                        //printf("interrupt ep up ok\n");
                        ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_NAK( ch374_read_byte( REG_USB_ENDP1 ) ) ^ BIT_EP1_TRAN_TOG );
                        break;
                    }
                    case USB_INT_EP0_SETUP: {  // 控制传输
                        USB_SETUP_REQ	SetupReqBuf;
                        usb_len = ch374_read_byte( REG_USB_LENGTH );
                        //printf("control translate usb_len = %d\n", usb_len);
                        int i;
                        
                        if ( usb_len == sizeof( USB_SETUP_REQ ) ) {
                            ch374_read_bytes( RAM_ENDP0_RECV, (uint8_t *)&SetupReqBuf, usb_len);
                            for(i = 0;i < usb_len;i++){
                                //printf("0x%02x ", *((uint8_t *)&SetupReqBuf + i));
                            }
                            //printf("\n");

                            SetupLen = SetupReqBuf.wLengthL;
                            if ( SetupReqBuf.wLengthH || SetupLen > 0x7F ) {
                                SetupLen = 0x7F;  // 限制总长度
                            }
                            usb_len = 0; // 默认为成功并且上传0长度
                            if ( ( SetupReqBuf.bType & DEF_USB_REQ_TYPE ) != DEF_USB_REQ_STAND ) {  /* 只支持标准请求 */
                                usb_len = 0xFF;  // 操作失败
                            }
                            else {  // 标准请求
                                //printf("standard request:");
                                SetupReq = SetupReqBuf.bReq;  // 请求码
                                switch( SetupReq ) {
                                    case DEF_USB_GET_DESCR:
                                        switch( SetupReqBuf.wValueH ) {
                                            case 1:
                                                //printf("get device desc\n");
                                                pDescr = (uint8_t *)( &MyDevDescr[0] );
                                                usb_len = sizeof( MyDevDescr );
                                                break;
                                            case 2:
                                                //printf("get config desc\n");
                                                pDescr = (uint8_t *)( &MyCfgDescr[0] );
                                                usb_len = sizeof( MyCfgDescr );
                                                break;
                                            case 3:
                                                switch( SetupReqBuf.wValueL ) {
                                                    //printf("get string desc, value = %d\n", SetupReqBuf.wValueL);
                                                    case 1:
                                                        pDescr = (uint8_t *)( &MyManuInfo[0] );
                                                        usb_len = sizeof( MyManuInfo );
                                                        break;
                                                    case 2:
                                                        pDescr = (uint8_t *)( &MyProdInfo[0] );
                                                        usb_len = sizeof( MyProdInfo );
                                                        break;
                                                    case 0:
                                                        pDescr = (uint8_t *)( &MyLangDescr[0] );
                                                        usb_len = sizeof( MyLangDescr );
                                                        break;
                                                    default:
                                                        usb_len = 0xFF;  // 操作失败
                                                        break;
                                                }
                                                break;
                                            case USB_REPORT_DESCR_TYPE: 
                                                //printf("get report desc\n");
                                                pDescr = (uint8_t *)( &ReportDescr[0] );
                                                usb_len = sizeof( ReportDescr );
                                                is_fake_km_ready = true;
                                                //xTaskCreate(keyboard_with_mouse_task, "test_task", 4096, NULL, 12, NULL);
                                                break;
                                            default:
                                                //printf("get desc: defautl\n");
                                                usb_len = 0xFF;  // 操作失败
                                                break;
                                        }
                                        if ( SetupLen > usb_len ) SetupLen = usb_len;  // 限制总长度
                                        usb_len = SetupLen >= RAM_ENDP0_SIZE ? RAM_ENDP0_SIZE : SetupLen;  // 本次传输长度
                                        ch374_write_bytes( RAM_ENDP0_TRAN, pDescr, usb_len);  /* 加载上传数据 */
                                        SetupLen -= usb_len;
                                        pDescr += usb_len;
                                        break;
                                    case DEF_USB_SET_ADDRESS:
                                        //printf("set addr\n");
                                        device_addr = SetupReqBuf.wValueL;  // 暂存USB设备地址
                                        break;
                                    case DEF_USB_GET_CONFIG:
                                        //printf("get config\n");
                                        ch374_write_byte( RAM_ENDP0_TRAN, UsbConfig );
                                        if ( SetupLen >= 1 ) usb_len = 1;
                                        break;
                                    case DEF_USB_SET_CONFIG:
                                        //printf("set config\n");
                                        UsbConfig = SetupReqBuf.wValueL;
                                        break;
                                    case DEF_USB_CLR_FEATURE:
                                        //printf("clear feature\n");
                                        if ( ( SetupReqBuf.bType & 0x1F ) == 0x02 ) {  // 不是端点不支持
                                            switch( SetupReqBuf.wIndexL ) {
                                                case 0x82:
                                                    ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_NAK( ch374_read_byte( REG_USB_ENDP2 ) ) );
                                                    break;
                                                case 0x02:
                                                    ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_RECV_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) );
                                                    break;
                                                case 0x81:
                                                    ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_NAK( ch374_read_byte( REG_USB_ENDP1 ) ) );
                                                    break;
                                                case 0x01:
                                                    ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_RECV_ACK( ch374_read_byte( REG_USB_ENDP1 ) ) );
                                                    break;
                                                default:
                                                    usb_len = 0xFF;  // 操作失败
                                                    break;
                                            }
                                        }
                                        else usb_len = 0xFF;  // 操作失败
                                        break;
                                    case DEF_USB_GET_INTERF:
                                        //printf("get interf\n");
                                        ch374_write_byte( RAM_ENDP0_TRAN, 0 );
                                        if ( SetupLen >= 1 ) usb_len = 1;
                                        break;
                                    case DEF_USB_GET_STATUS:
                                        //printf("get status\n");
                                        ch374_write_byte( RAM_ENDP0_TRAN, 0 );
                                        ch374_write_byte( RAM_ENDP0_TRAN + 1, 0 );
                                        if ( SetupLen >= 2 ) usb_len = 2;
                                        else usb_len = SetupLen;
                                        break;
                                    default:
                                        //printf("default!!!\n");
                                        usb_len = 0xFF;  // 操作失败
                                        break;
                                }
                            }
                        }
                        else {
                            usb_len = 0xFF;  // 操作失败
                        }
                        if ( usb_len == 0xFF ) {  // 操作失败
                            ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_RECV_STA( M_SET_EP0_TRAN_STA( 0 ) ) );  // STALL
                        }
                        else if ( usb_len <= RAM_ENDP0_SIZE ) {  // 上传数据
                            ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_ACK( M_SET_EP0_RECV_ACK( ch374_read_byte( REG_USB_ENDP0 ) ), usb_len ) | BIT_EP0_TRAN_TOG );  // DATA1
                        }
                        else {  // 下传数据或其它
                            ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_NAK( M_SET_EP0_RECV_ACK( ch374_read_byte( REG_USB_ENDP0 ) ) ) | BIT_EP0_RECV_TOG );  // DATA1
                        }
                        break;
                    }

                    case USB_INT_EP0_IN: {
                        //printf("USB_INT_EP0_IN\n");
                        //uint8_t len;
                        switch( SetupReq ) {
                            case DEF_USB_GET_DESCR:
                                usb_len = SetupLen >= RAM_ENDP0_SIZE ? RAM_ENDP0_SIZE : SetupLen;  // 本次传输长度
                                ch374_write_bytes( RAM_ENDP0_TRAN, pDescr, usb_len);  /* 加载上传数据 */
                                SetupLen -= usb_len;
                                pDescr += usb_len;
                                ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_ACK( ch374_read_byte( REG_USB_ENDP0 ), usb_len ) ^ BIT_EP0_TRAN_TOG );
                                break;
                            case DEF_USB_SET_ADDRESS:
                                ch374_write_byte( REG_USB_ADDR, device_addr );
                            default:
                                ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_NAK( 0 ) );  // 结束
                                break;
                        }
                        break;
                    }

                    case USB_INT_EP0_OUT: {
                        //printf("USB_INT_EP0_OUT\n");
                        switch( SetupReq ) {
        //					case download:
        //						get_data;
        //						break;
                            case DEF_USB_GET_DESCR:
                            default:
                                ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_NAK( 0 ) );  // 结束
                                break;
                        }
                        break;
                    }

                    default:
                        //printf("default\n");
                    break;
                }

                ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_TRANSFER );  // 清中断标志 
                if(ep2_in_intr_flag == true) {
                    ep2_in_intr_flag = false;
                    //xEventGroupSetBits(Event_Handle, EP2_IN_EVENT);
                    xHigherPriorityTaskWoken = pdFALSE;
                    xResult = xEventGroupSetBitsFromISR(Event_Handle, EP2_IN_EVENT, &xHigherPriorityTaskWoken);
                    if(xResult != pdFAIL){
                        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
                    }
                }
            }else if ( int_status & BIT_IF_USB_SUSPEND ) {  // USB总线挂起
                //printf("usb bus suspend\n");
                ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_USB_SUSPEND );  // 清中断标志
                ch374_write_byte( REG_SYS_CTRL, ch374_read_byte( REG_SYS_CTRL ) | BIT_CTRL_OSCIL_OFF );  // 时钟振荡器停止振荡,进入睡眠状态
            }else if ( int_status & BIT_IF_WAKE_UP ) {  // 芯片唤醒完成
                //printf("wake up\n");
                ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_WAKE_UP );  // 清中断标志
            }else {  // 意外的中断,不可能发生的情况,除了硬件损坏
                //printf("hardware error\n");
                ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_INTER_FLAG );  // 清中断标志
            }
        }
        //vTaskDelay(20 / portTICK_PERIOD_MS);  //关闭了watch dog
    }
        
    //vTaskDelete(NULL);
}

void km_report(struct report_msg msg)
{
    if(is_fake_km_ready == true) {
        xQueueSend(xQueue, &msg, portMAX_DELAY);
    }
}

void usb_report_task(void *parm)
{
    BaseType_t xReturn=pdTRUE;
    struct report_msg msg;
    uint8_t *ptr;

    uint8_t i;

    xEventGroupSetBits(Event_Handle, EP2_IN_EVENT);

    while(1)
    {
        xReturn=xQueueReceive(xQueue, &msg, portMAX_DELAY);
        if(pdTRUE==xReturn){
            ptr = (uint8_t *)&msg;
            for(i = 0;i < sizeof(struct report_msg);i++){
                printf("%02x ", ptr[i]);
            }
            printf("\n");

            xEventGroupWaitBits(Event_Handle, /* 事件对象句柄 */ 
                              EP2_IN_EVENT,/* 接收任务感兴趣的事件 */ 
                              pdTRUE, /* 退出时清除事件位 */ 
                              pdTRUE, /* 满足感兴趣的所有事件 */ 
                              portMAX_DELAY);/* 指定超时事件,一直等 */ 

            ch374_write_bytes( RAM_ENDP2_TRAN, (uint8_t *)&msg, 9);
            ch374_write_byte( REG_USB_LENGTH, 9 );
            ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_ACK( ch374_read_byte( REG_USB_ENDP2 ) ) ^ BIT_EP2_RECV_TOG );
        }
        else{
            printf("xQueueReceive error!\n");
        }
 
    }
}

void ch374_device_init(void)
{
    vTaskDelay(500 / portTICK_PERIOD_MS);

    xQueue = xQueueCreate(1024, sizeof(struct report_msg));
    Event_Handle = xEventGroupCreate(); 
    if (NULL != Event_Handle) 
    {
        printf("Event_Handle create success\r\n"); 
    }

    ch374_device_port_init();

    //xTaskCreate(usb_device_task, "usb_device_task", 8192, NULL, 12, NULL);
    xTaskCreate(usb_report_task, "usb_report_task", 8192, NULL, 12, NULL);

    ch374_write_byte( REG_USB_SETUP, 0x00 );
	ch374_write_byte( REG_USB_ADDR, 0x00 );
	ch374_write_byte( REG_USB_ENDP0, M_SET_EP0_TRAN_NAK( 0 ) );
	ch374_write_byte( REG_USB_ENDP1, M_SET_EP1_TRAN_NAK( 0 ) );
	ch374_write_byte( REG_USB_ENDP2, M_SET_EP2_TRAN_NAK( 0 ) );
	ch374_write_byte( REG_INTER_FLAG, BIT_IF_USB_PAUSE | BIT_IF_INTER_FLAG );  // 清所有中断标志
	ch374_write_byte( REG_INTER_EN, BIT_IE_TRANSFER | BIT_IE_BUS_RESET | BIT_IE_USB_SUSPEND );  // 允许传输完成中断和USB总线复位中断以及USB总线挂起中断,芯片唤醒完成中断
	ch374_write_byte( REG_SYS_CTRL, BIT_CTRL_OE_POLAR | BIT_CTRL_CLK_12MHZ );  // 对于CH374T或者UEN引脚悬空的CH374S必须置BIT_CTRL_OE_POLAR为1   使用12M晶振
	ch374_write_byte( REG_USB_SETUP, BIT_SETP_TRANS_EN | BIT_SETP_PULLUP_EN );  // 启动USB设备

}