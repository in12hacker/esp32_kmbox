#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_wifi.h"


extern void esp32_ota(void);
extern void connect_wifi(void);
extern void mqtt_start(void);
extern void adc_test(void);
extern void ch374_device_init(void);
extern void usb_hub_task(void * arg);

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
#if 0
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    connect_wifi();
#endif

    ch374_device_init();

    xTaskCreate(usb_hub_task, "usb_hub_task", 4 * 4096, NULL, 12, NULL);
}