#ifndef __CH374_APP_H__
#define __CH374_APP_H__

#include "ch374_usb.h"

#define CLEAR_HUB_FEATURE	0x20
#define CLEAR_PORT_FEATURE	0x23
#define GET_BUS_STATE		0xa3
#define GET_HUB_DESCRIPTOR	0xa0
#define GET_HUB_STATUS		0xa0
#define GET_PORT_STATUS		0xa3
#define SET_HUB_DESCRIPTOR	0x20
#define SET_HUB_FEATURE		0x20
#define SET_PORT_FEATURE	0x23

//////Hub Class Feature Selectors
#define	C_HUB_LOCAL_POWER	0
#define C_HUB_OVER_CURRENT	1
#define PORT_CONNECTION		0
#define PORT_ENABLE			1
#define PORT_SUSPEND		2
#define PORT_OVER_CURRENT	3
#define PORT_RESET			4
#define PORT_POWER			8
#define	PORT_LOW_SPEED		9
#define C_PORT_CONNECTION	16
#define C_PORT_ENABLE		17
#define C_PORT_SUSPEND		18
#define C_PORT_OVER_CURRENT	19
#define C_PORT_RESET		20

////////Hub Class Request Codes
#define GET_STATUS			0
#define CLEAR_FEATURE		1
#define GET_STATE			2
#define SET_FEATURE			3
#define GET_DESCRIPTOR		6
#define SET_DESCRIPTOR		7

#define DEV_ERROR 0x00
#define DEV_KEYBOARD 0x31
#define DEV_MOUSE 0x32
#define DEV_PRINT 0x70
#define DEV_DISK 0x80
#define DEV_HUB 0x90
#define DEV_ADB 0xA0
#define DEV_UNKNOWN 0xFF

typedef struct _HUB_DESCRIPTOR
{
	unsigned char bDescLength;
	unsigned char bDescriptorType;
	unsigned char bNbrPorts;
	unsigned char wHubCharacteristics[2];
	unsigned char bPwrOn2PwrGood;
	unsigned char bHubContrCurrent;
	unsigned char DeviceRemovable;
	
}
HUBDescr,*PHUBDescr;

typedef struct _INF
{
	unsigned char bAddr;
	unsigned char bDevType; 
	unsigned char bUpPort; 
	unsigned char bEndpSize;
	
	union _KUNO
	{
		struct _HUB
		{
			unsigned char bNumPort;
			unsigned char bHUBendp;
			unsigned char bInterval;
			unsigned char bSlavePort[7];
		}
		HUB; // 集线器属�?

		struct _DEV
		{
			unsigned char bSpeed;

		}
		DEV; 
	}
	KUNO; 
}
INF, *PINF; 

typedef struct _NUM
{
	INF Num[50]; 
}
NUM, *PNUM;

typedef struct _RootHubDev
{
	uint8_t DeviceStatus;  // 设备状态,0-无设备,1-有设备但尚未初始化,2-有设备但初始化枚举失败,3-有设备且初始化枚举成功
	uint8_t DeviceAddress; // 设备被分配的USB地址
	uint8_t DeviceSpeed;   // 0为低速,非0为全速
	uint8_t DeviceType;	// 设备类型
						   //	union {
						   //		struct MOUSE {
						   //			uint8_t	MouseInterruptEndp;		// 鼠标中断端点号
						   //			uint8_t	MouseIntEndpTog;		// 鼠标中断端点的同步标志
						   //			uint8_t	MouseIntEndpSize;		// 鼠标中断端点的长度
						   //		}
						   //		struct PRINT {
						   //		}
						   //	}
						   //.....    struct  _Endp_Attr   Endp_Attr[4];	//端点的属性,最多支持4个端点
	uint8_t GpVar;		   // 通用变量
	uint8_t Endp_In;
	uint8_t Endp_Out;
	USB_DEV_DESCR dev_descr;
	USB_CFG_DESCR cfg_descr;
	bool send_tog_flag;
	bool recv_tog_flag;
}S_RootHubDev;

typedef struct _DevOnHubPort
{
	uint8_t DeviceStatus;  // 设备状态,0-无设备,1-有设备但尚未初始化,2-有设备但初始化枚举失败,3-有设备且初始化枚举成功
	uint8_t DeviceAddress; // 设备被分配的USB地址
	uint8_t DeviceSpeed;   // 0为低速,非0为全速
	uint8_t DeviceType;	// 设备类型
						   //.....    struct  _Endp_Attr   Endp_Attr[4];	//端点的属性,最多支持4个端点
	uint8_t GpVar;		   // 通用变量
}S_DevOnHubPort;

#define	HUB_TYPE		0x66
#define FUNCTION_DEV	0x77
#define	FULL_SPEED		0x88
#define LOW_SPEED		0x99
#define FIND_ATTACH		0xaa
#define FIND_REMOVE		0xbb

//USB设备相关信息表，CH374U最多支持3个设备
#define ROOT_DEV_DISCONNECT 0
#define ROOT_DEV_CONNECTED 1
#define ROOT_DEV_FAILED 2
#define ROOT_DEV_SUCCESS 3


bool Query374Interrupt(uint8_t *inter_flag_reg);
void HostDetectInterrupt(uint8_t inter_flag_reg);
void Init374Host(void);
void NewDeviceEnum(void);
void AnalyzeRootHub(void);
void DeviceLoop(void);




#endif
