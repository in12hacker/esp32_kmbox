# esp32_kmbox

### 介绍
需求：kmbox用户手册 版本20230307.pdf
硬件是抄板，软件上要兼容竞品上位机命令


### 硬件设计
#### 0.硬件连接
主控芯片是esp32-solo-1，本身没有usb借口，使用了2颗ch374u芯片进行usb转spi，esp32需要使用gpio模拟spi主设备
CH374 的 SPI 接口支持 SPI 模式 0 和 SPI 模式 3，CH374 总是从 SPI 时钟 SCK 的上升沿输入数据，
并在允许输出时从 SCK 的下降沿输出数据
原理图
![Alt text](image.png)
受控PC的USB口连接的U6那个ch374u：       CS-IO16  CLK-IO17  MOSI-IO5  MISO-IO18  INT#-IO4
两个鼠标键盘的USB口连接的U4那个ch374u：   CS-IO25  CLK-IO26  MOSI-IO27  MISO-IO14  kmbox接鼠标键盘的这个ch374不需要中断脚，本质上是轮询
OLED(未用)：IO32  IO33  IO19  IO21  IO22

#### 1. 下载程序
USB连接kmbox串口USB端口，即可下载，不用管IO0：idf.py -p COM3 flash monitor

### 软件架构
软件架构说明
#### 开发环境
esp-idf4.4

#### 1. ota
1）make menuconfig 设置wifi name和password，设置固件下载地址为：http://192.168.0.103:8070/hello_world.bin（注意是http），[*] Skip server certificate CN fieldcheck
   这里注意ip要和起服务的电脑的ip一致。

2）路由器wifi不能设置为5G和2.4G混合，要分开，然后链接2.4G的。

3）在pc端，bin文件夹路径下右键打开shell，执行：python -m http.server 8070

### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

### 记录

1.  有的键盘可能有多个接口描述符,ParseConfigDescr解析配置描述符的时候，注意itf_count < 多少，目前手动设置了只解析一个接口描述符.
2.  将usb_device_task换成中断
3.  目前还会出现鼠标和键盘一起差上，同时操作，模拟km会掉或者，一直按着某个键不放的现象，无线鼠标不对
4.  374内部要保护，不然2个任务切换的时候，容易出现混乱现象。

